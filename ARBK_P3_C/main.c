/*
 * ARBK_P3_C.c
 *
 * Created: 25.10.2017 09:22:41
 * Author : Fabian
 */

#include <avr/io.h>
#include <avr/interrupt.h>

// interrupt routine
ISR(TIMER0_OVF_vect) {
    PORTD ^= (1 << 7) | (1 << 6);
}

// main
int main(void) {
    // init
    DDRD |= (1 << 7) | (1 << 6);
    PORTD |= (1 << 7) | (0 << 6);

    // timer
    TCCR0 |= (1 << CS02) | (1 << CS00);
    TIMSK |= (1 << TOIE0);

    // enable interrupts
    sei();

    while (1) {}
}

