; #############################################################################
; ARBK_P3_ASM.asm
;
; ARBK - FH Aachen - WS2017/2018
;
; Created: 25.10.2017
; Author : Fabian Sch�ttler
; #############################################################################
;
;
; #############################################################################
.INCLUDE <m8def.inc>   ; includes controller labels
.CSEG                   ; start code segment

; CPU Frequency (Hz)
.equ f_cpu = 1000000

; define some labels for working registers
.def temp = r16
.def leds = r17

; set shift limits
.equ led_mask = 0xC0

; Interrupt Vector Table
.org 0x0000
    rjmp    init        ; Reset vector

.org OVF0addr
    rjmp    timer0_ovf

; reserve progmem for full vectortable
.org INT_VECTORS_SIZE

; begin of main routine
init:
    ; init stack
    ldi     temp, HIGH(RAMEND)
    out     SPH, temp
    ldi     temp, LOW(RAMEND)
    out     SPL, temp

    ; init PortD
    ; set PD7, PD6 as output
    in      temp, DDRD
    ori     temp, led_mask
    out     DDRD, temp
    ; set PD76 as high (inverted logic for leds), PD6 on, PD7 off
  /*  in      leds, PORTD
    ori     leds, (1 << 6)
    out     PORTD, leds*/
    sbi     PORTD, 6

    ;init timer
    in      temp, TCCR0
    ori     temp, (1 << CS02) | (1 << CS00)     ; prescaler 1024
    out     TCCR0, temp
    in      temp, TIMSK
    ori     temp, (1 << TOIE0)                  ; enable overflow isr
    out     TIMSK, temp

    sei

; entry for main loop
loop:
    rjmp    loop


timer0_ovf:
    in      r20, SREG
    push    r20

    in      leds, PORTD
    ldi     temp, led_mask          ;
    eor     leds, temp              ; toggle bits in leds
    out     PORTD, leds             ; write leds to port

    pop     r20
    out     SREG, r20
    reti
